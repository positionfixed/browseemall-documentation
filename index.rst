BrowseEmAll Documentation
=====================================================

`Homepage <https://www.browseemall.com>`_ |
`Download <https://www.browseemall.com/Try>`_ |
`Support <https://www.browseemall.com/Support>`_ |
`Release Notes <https://www.browseemall.com/Support/ReleaseNotes>`_

BrowseEmAll is a client side cross browser testing tool. It runs natively on Windows, macOS and Linux and can be used to test for all major desktop and mobile browsers. You can use it directly on your local machine and in your local network.

.. toctree::
   :maxdepth: 2

   installation
   manual
   automation
   settings
   commandline