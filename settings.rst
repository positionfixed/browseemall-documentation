Settings
========

BrowseEmAll gives you the option to change a few different settings inside the applications settings menu. The options available are explained after the image.

.. image:: _static/settings.png

Check For Updates
-----------------

With this button you can manually check if a new version of BrowseEmAll is available. Generally the application will always check for a new version on launch.

Enter Licence Key
-----------------

Can be used to change or update your licence key.

Open Log Folder
---------------

Will open the folder that contains the BrowseEmAll logfiles. This is useful to provide our support team with the logfiles in case of a problem.

Delete Virtual Machines
-----------------------

Deletes all virtual machines created by BrowseEmAll.

Change Browser Storage Location
-------------------------------

Defines the location on the hard disk where virtual machines and browser executables are stored. Change this if these files should not be part of your user profile.

Disable Usage Statistics
------------------------

Disables to collection of anonymous usage statistics which can help us improve the application.

Download All Browsers
---------------------

If you want to use BrowseEmAll in an offline environment you can download all browsers at once. 

.. note::
    Depending on your network speed this can take a few hours. Around 50 GB of free disk space will be used.

Proxy
-----

Useful to manually config a http and https proxy if the correct on is not picked up automatically.

Resolutions
-----------

Can be used to add custom resolutions for quick responsive testing.